// console.log("s23");


/* [SECTION] Objects
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
    - Different data types may be stored in an object's property creating complex data structures
*/
// Creating objects using object initializers /literal notatio
/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/
//example of initialization:
let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log('Result from creating objects using initializers/literal notation:');
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
}
let laptop = new Laptop("Lenovo", 2018);
console.log("Result from creating objects using object constructors: ");
console.log(laptop);

let myLaptop = new Laptop("MacBook Air", 2020);
console.log("Result from creating objects using object constructors: ");
console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Result from creating objects without the new keyword:');
console.log(oldLaptop);

let myNewLaptop = new Laptop('Nitro 5', 2000);
console.log('Result from creating objects without the new keyword:');
console.log(myNewLaptop);
// make sure to use new keyword to store data / values successfully;

// Creating empty objects
let computer = { };
let myComputer = new Object();

console.log("-------------");
console.log("Accessing Object Properties");
// [SECTION] Accessing Object Properties

// Using dot notation
console.log("Result from dot notation: " +myLaptop.name);
console.log("Result from dot notation: " +cellphone.name);

//Using the square bracket notation
console.log("Result from square bracket: " +myNewLaptop['manufactureDate']);

console.log("-------------");
console.log("Accessing Object value inside an array");
//objects inside an array
let array = [laptop, myNewLaptop];
console.log(array[0]['name']);
console.log(array[0].name);


console.log("-------------");
console.log("Initializing an object property");
// [Section] Initializing/Adding/Deleting/Reassigning Object Properties
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/
    
    let car = {};

    //initialization using dot notation
    car.name = "Honda Civic";
    console.log("Result from adding properties using dot notation: ");
    console.log(car);


    //initialization using square bracket
    car['manufacture date'] = 2019;
    console.log(car['manufacture date']);
    console.log(car['Manufacture Date']);// undefined - case sensitive
    console.log(car.manufactureDate); // undefined. it is much effective to use square bracket to access property name with spaces

    console.log("Result from adding properties using square bracket notation")
    console.log(car);


console.log("-------------");
console.log("Deleting object property");

delete car['manufacture date'];
console.log("Result from Deleting properties: ");
console.log(car);

console.log("-------------");
console.log("Reassigning object property");

//Reassigning through dot notation
/*car.name = "Dodge Charger R/T";
console.log("Result from reassigning properties")
console.log(car);*/

//Reassigning through sq bracket
car['name'] = "Dodge Charger R/T";
console.log("Result from reassigning properties")
console.log(car);


console.log("-------------");
console.log("Object Method");

// [Section] Object Methods
/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/
//function inside an object property
    let person = {
        name: 'John',

        //this is a method (talk)
        talk: function(){
            console.log("Hello, my name is " +this.name); // we use 'this' keyword to access a property that co-exist inside
        }
    }

    console.log(person);
    console.log("Result from object methods: ");
    person.talk();


//We can also create a method outside the object code - example (walk)
// Adding methods to objecs
/* Syntax for initializing / reassigning a property
        'person' - object
        'walk' - new property
*/
    //initializing / Create new property
    person.walk = function(){
        console.log(this.name + ' walked 25 steps forward');
    }
    
    person.walk();

//reassigning
person.walk = function(){
        console.log(this.name + ' walked 30 steps forward');
    }
    
    person.walk();

    console.log(person);
    console.log(person.walk);

// Methods are useful for creating reusable functions that perform tasks related to objects

/* Scenario:
    1. We would like to create a game that would have several pokemon interact with each other.
    2. Every pokemon would have the same set of stats, properties and functions.
*/

    function Pokemon(name, level){

    //Properties
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + "tackled" + target.name);
        
        target.health -= this.attack
        console.log(target.name + " health is now reduceed to "+target.health);

        if(target.health <= 0){
            target.faint();
        }
    }
    this.faint = function(){
        console.log(this.name + " fainted ");
    }
}

let pikachu = new Pokemon("Pikachu", 99);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 5);
console.log(rattata);

pikachu.tackle(rattata);
rattata.tackle(pikachu);